# Week5 Mini-Project

## Introduction
In this project, I created a Rust AWS Lambda function with AWS RDS database(MySQL). My AWS Lambda function is designed to handle user registration requests. It receives JSON data containing a username and an email, checks against a MySQL database for existing records, and either registers the new user or returns messages to indicate the user already exists.

## Walkthrough

### Create database instance in AWS
Go to the RDS service in AWS Console and create a new databse instance. I choose MySQL as the database engine.
- Remember to modify the security group to allow the inbound traffic from your IP address.

### Connect to AWS MySQL
After creating the database instance, I tried to connect to it from my local terminal. I use the following command to connect to this MySQL database:

```
mysql -h <HOST> -P <PORT> -u <YOUR_USER_NAME> -p
```
- connect locally
![mysql_local](images/mysql_local.png)

### Create and initialize table
Create a table named 'users' and insert some data into it.
- The table I just created
![image](images/sqldata.png)
Now we have a table named 'users' with two columns: 'username' and 'email'. The AWS database works as expected. My AWS Lambda function will check against this table to see if the user already exists.

### Connect to database and test the lambda function locally
Use `cargo build` and `cargo watch` to build and run the project locally. Then I use Postman to test the lambda function. The following is the result of the test.
![image](images/postman.png)

### Deploy lambda function to AWS
- Use `cargo lambda build` and `cargo lambda deploy` to deploy the lambda function to AWS. The following is the result of the deployment.
![lambda](images/lambda.png)

### Test the lambda function in AWS
- Add a API Gateway trigger to the lambda function.
![image](images/api.png)
- Use Postman to test the lambda function in AWS. The following is the result of the test.
- add a new user
![image](images/apitest.png)
- try to add the same user again
![image](images/apitest2.png)
- check the database to see if the user is added
![image](images/final.png)
